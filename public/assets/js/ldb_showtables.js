var DbdModalShowTables = function (app) {
    var modalSelector = '#modal-show-tables';

    var form = $(modalSelector);
    var header = form.find(modalSelector + '-title');
    var btnClose = form.find(modalSelector + '-btn-close');
    var btnCancel = form.find(modalSelector + '-btn-cancel');
    var alert = form.find(modalSelector+'-alert');

    var showAlert = function(message,type){
        alert.empty()
            .attr('class','alert alert-'+type)
            .html(message);
    };

    var clearAlert = function(){
        alert.empty()
            .attr('class','');
    };

    form.modal({
        show: false
    });

    var btnCloseClick = function () {
        clearAlert();

        form.modal('hide');
    };

    btnClose.click(btnCloseClick);
    btnCancel.click(btnCloseClick);

    app.addFunction('listExistsTablesShowAll',function(show){
        var tables = this.existsTables.peek();
        each(tables, function (item, index) {
            var item = new ItemListExistsTables(tables[index].name, tables[index].check,
                tables[index].inDiagram, true);
            this.existsTables.replace(tables[index],item);
        },this);
    });

    app.addFunction('listExistsTablesEnableOrDisableAll',function(enable){
        var tables = this.existsTables.peek();
        each(tables, function (item, index) {
            var item = new ItemListExistsTables(tables[index].name, enable,
                tables[index].inDiagram, tables[index].show);
            this.existsTables.replace(tables[index],item);
        },this);
    });

    app.addFunction('listExistsTablesShowOrHideEnable',function(show){
        var tables = this.existsTables.peek();
        each(tables, function (item, index) {
            var item = new ItemListExistsTables(tables[index].name, tables[index].check,
                tables[index].inDiagram, tables[index].check ? show : (!show));
            this.existsTables.replace(tables[index],item);
        },this);
    });

    return {
        show : function (doc, tables, existTables, error) {
            $.ajax({
                url: '/json/tables',
                data: {id: doc},
                dataType: 'json',
                async: false,
                error: error,
                success: function (data) {
                    var tablesData = [];
                    for (var i = 0, j = data.length; i<j; ++i) {
                        var inDiagram = (existTables.indexOf(data[i]) >= 0);
                        tablesData.push(new ItemListExistsTables(data[i], inDiagram, inDiagram, true));
                    }

                    tables(tablesData);

                    form.modal('show');
                }
            })
        },

        close : btnCloseClick,

        getDefs : function(docId, tables, sucTablesDefsCallback, sucIndexesDefsCallback, sucLinksDefsCallback){
            var self = this;

            var callbackErr = function(jqXHR,textStatus,errorThrown){
                if (typeof jqXHR.responseJSON === "undefined") {
                    showAlert(jqXHR.statusText,'danger');
                } else {
                    showAlert(jqXHR.responseJSON.error,'danger');
                }
            };

            var ajaxTablesDefs = function(){
                return $.ajax ({
                    url:'/json/tables_defs',
                    data: {id:docId,tables:tables},
                    dataType: 'json',
                    type: 'POST'
                })
            };

            var ajaxIndexesDefs = function(){
                return $.ajax ({
                    url:'/json/indexes',
                    data: {id:docId,tables:tables},
                    dataType: 'json',
                    type: 'POST'
                })
            };

            var ajaxLinksDefs = function(){
                return $.ajax({
                    url:'/json/links',
                    data: {id:docId,tables:tables},
                    dataType:'json',
                    type:'POST'
                })
            };

            $.when(ajaxTablesDefs(), ajaxIndexesDefs(), ajaxLinksDefs())
                .done(function(tablesDefs,indexesDefs,linksDefs){
                    sucTablesDefsCallback(tablesDefs[0]);
                    sucIndexesDefsCallback(indexesDefs[0]);
                    sucLinksDefsCallback(linksDefs[0]);

                    clearAlert();
                    self.close();
                })
                .fail(callbackErr);
        }
    }
};