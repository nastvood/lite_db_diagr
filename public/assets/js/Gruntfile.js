module.exports=function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '// <%= pkg.name %>.js\n// (c) <%= grunt.template.today("yyyy")%> <%= pkg.author %>  \n '
            },
            build: {
                src: ['dbdiagr.js'],
                dest: 'dbdiagr.min.js'
            }
        },
        jshint:{
            options:{
                curly:true,
                eqeqeq:true,
                latedef:true,
                noempty:true,
                unused:true
            },
            all:['dbdiagr.js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('default',['jshint']);
    grunt.registerTask('build',['uglify']);
}
