var Field = function(context,config,fieldDescr){
    var self = this;

    self.name = fieldDescr.name || "";

    self.dataType = fieldDescr.dataType.toUpperCase() || "";
    self.dataLen = fieldDescr.dataLen || "0";
    self.pk = fieldDescr.pk || false;

    self.x=0;
    self.y=0;
    self.width=0;
    self.height=0;

    var getTypeDescr = function(){
        return self.dataType + (self.dataLen === '0'?'':'('+self.dataLen+')');
    };

    var getPKDescr = function(){
        return self.pk ? 'PK' : '';
    };

    var metricsNameWidth = 0;
    var strNameLen = 0;
    var metricsTypeWidth = 0;
    var strTypeDescr = 0;
    var metricsPKWidth = 0;
    var strPKDescr = 0;

    self.getMinWidth=function(){
        if (strNameLen !== self.name.length){
            metricsNameWidth = config.table.textPadding * 2 + context.measureText(self.name).width;
        }

        if (strTypeDescr !== getTypeDescr().length){
            metricsTypeWidth = config.table.textPadding * 2 + context.measureText(getTypeDescr()).width;
        }

        if (strPKDescr !== getPKDescr().length){
            metricsPKWidth = config.table.textPadding * 2 + context.measureText(getPKDescr()).width;
        }

        return [
            metricsNameWidth,
            metricsTypeWidth,
            metricsPKWidth
        ]
    };

    self.draw = function(x, y, widthName, widthType, widthPK, height){
        self.x = x;
        self.y = y - config.table.textPadding - config.font.size;
        self.width = widthName + widthType + widthPK;
        self.height = height;

        context.fillStyle = config.field.pkColor;
        context.fillText(getPKDescr(), x + config.table.textPadding, y, widthType, height);

        context.fillStyle = config.field.nameColor;
        context.fillText(self.name, widthPK + x + config.table.textPadding, y, widthName, height);

        context.fillStyle = config.field.typeColor;
        context.fillText(getTypeDescr(), widthPK + widthName + x + config.table.textPadding, y, widthType, height);
    };

    self.toObject = function(){
        return {
            name:self.name,
            dataType:self.dataType,
            dataLen:self.dataLen,
            pk:self.pk
        }
    };
};