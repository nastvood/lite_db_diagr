var dbdAuthForm = (function(){
    var modalSelector='#modal-auth';

    var form = $(modalSelector);
    var header=form.find(modalSelector+'-title');
    var btnOk=form.find(modalSelector+'-btn-ok');
    var btnClose=form.find(modalSelector+'-btn-close');
    var btnCancel=form.find(modalSelector+'-btn-cancel');

    var email=form.find(modalSelector+'-form-input-email');
    var pass=form.find(modalSelector+'-form-input-pass');

    var resultClick=false;
    var response=null;

    form.modal({
        show:false
    });

    form.on('shown.bs.modal', function(e){
        email.focus();
    });

    var btnOkClick=function(e){
        response({email:email.val(),
            pass:pass.val()
        });

        form.modal('hide');
    };

    var btnCloseClick=function(e){
        response(null);

        form.modal('hide');
    };

    btnOk.click(btnOkClick);
    btnClose.click(btnCloseClick);
    btnCancel.click(btnCloseClick);

    return {
        show : function(title,rescallback){
            response=rescallback;

            header.empty()
                .append('<strong>'+title+'</strong>');

            form.modal('show');

            email.val('');
            pass.val('');
        },

        signup : function(success,error){
            this.show('Sign up',function(result){
                if (result){
                    $.ajax({
                        url:'/auth/signup',
                        data:result,
                        dataType:'json',
                        async:false,
                        error: error,
                        success:success
                    })
                }
            })
        },

        logout : function(success,error){
            $.ajax({
                url:'/auth/logout',
                dataType:'json',
                async:false,
                error: error,
                success:success
            });
        },

        login : function(success,error){
            this.show('Log in',function(result){
                if (result){
                    $.ajax({
                        url:'/auth/login',
                        data:{email:result.email,pass:result.pass},
                        dataType:'json',
                        async:false,
                        error: error,
                        success:success
                    })
                }
            });
        }
    }
})();
