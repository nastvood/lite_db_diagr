var dbdMessages=new (function(){
    var WARNING = 'warning';
    var INFO = 'info';
    var SUCCESS = 'success';
    var DANGER = 'danger';

    var showMessage = function(title,mess,type){
        var alert = '#modal-message .alert';
        var header = '#modal-message .modal-header';

        $(alert).empty()
            .attr('class','alert alert-'+type+' alert-dismissable')
            .append('<span>'+mess+'</span>');;

        $(header).empty()
            .append('<span><strong>'+title+'</strong></span>');

        $('#modal-message').modal('show');
    };

    return {
        showWarning : function(title,mess){
            showMessage(title,mess,WARNING);
        },

        showInfo : function(title,mess){
            showMessage(title,mess,INFO);
        },

        showSuccess : function(title,mess){
            showMessage(title,mess,SUCCESS);
        },

        showDanger : function(title,mess){
            showMessage(title,mess,DANGER);
        }
    }
})();

var dbdConfirmMessage = (function (){
    var alertClass='alert alert-warning alert-dismissable';
    var modalSelector='#modal-confirm';
    var alertSelector=modalSelector+' .alert';
    var headerSelector=modalSelector+' .modal-header';
    var btnNoSelector=modalSelector+'-btn-no';
    var btnYesSelector=modalSelector+'-btn-yes';
    var resultClick=false;
    var response=null;

    $(modalSelector).modal({
        backdrop:'static',
        show:false
    });

    var btnNoClick=function(e){
        response(-1);
        $(modalSelector).modal('hide');
    }

    var btnYesClick=function(e){
        response(1);
        $(modalSelector).modal('hide');
    }

    $(btnNoSelector).click(btnNoClick);
    $(btnYesSelector).click(btnYesClick);

    return {
        confirm : function(title,mess,rescallback){
            response=rescallback;

            $(alertSelector).empty();
            $(headerSelector).empty();
            $(modalSelector).modal('show');
            $(headerSelector).append('<span><b>'+title+'</b></span>');
            $(alertSelector).append('<span>'+mess+'</span>');
        }
    }
})();

var dbdInputBox  = (function(){
    var modalSelector='#modal-input';
    var headerSelector=modalSelector+' #modal-input-title';
    var btnOkSelector=modalSelector+'-btn-ok';
    var btnCloseSelector=modalSelector+'-btn-close';
    var promptSelector=modalSelector+'-form-label';
    var inputSelector=modalSelector+'-form-input';
    var resultClick=false;
    var response=null;

    $(modalSelector).modal({
        show:false
    });

    var btnOkClick=function(e){
        response($(inputSelector).val());
        $(modalSelector).modal('hide');
    };

    var btnCloseClick=function(e){
        response(null);
        $(modalSelector).modal('hide');
    };

    $(btnOkSelector).click(btnOkClick);
    $(btnCloseSelector).click(btnCloseClick);

    return {
        showBox : function(title,prompt,defaultValue,rescallback){
            response=rescallback;

            $(headerSelector).empty();
            $(promptSelector).empty();
            $(modalSelector).modal('show');
            $(headerSelector).append('<span><b>'+title+'</b></span>');
            $(promptSelector).append('<span><b>'+prompt+'</b></span>');
            $(inputSelector).val(defaultValue);
        }
    }
})();


