var DbdModalShowDiagrs = function(app){
    var modalSelector='#modal-show-diagrs';

    var form = $(modalSelector);
    var header = form.find(modalSelector+'-title');
    var btnOpen = form.find(modalSelector+'-btn-open');
    var btnClose = form.find(modalSelector+'-btn-close');
    var btnCancel = form.find(modalSelector+'-btn-cancel');

    var resultClick=false;
    var response=null;

    form.modal({
        show:false
    });

    var btnCloseClick=function(){
        form.modal('hide');
    };

    btnClose.click(btnCloseClick);
    btnCancel.click(btnCloseClick);

    app.addFunction('removeDiagram',function(diagram){
        var mess =  'Remove diagram <b>' +  diagram.title + '<b>?';
        dbdConfirmMessage.confirm('Remove diagram', mess, function (result) {
            if (result > 0){
                console.log(diagram.id,diagram);
                /*ajaxPostJson('/json/remove_diagram', {id : diagram.id})
                    .done(function(){
                        this.diagrams.remove(diagram);
                    })
                    .fail(defaultCallbackErr); */
            }
        })
    });

    return {
        show : function(rescallback,error){
            ajaxPostJson('/json/diagrams')
                .done(rescallback)
                .fail(error);

            form.modal('show');
        },

        open : function(docId,rescallback,error){
            ajaxPostJson('/json/diagram', {id:docId})
                .done(rescallback)
                .fail(error);

            btnCloseClick();
        }
    }
};

