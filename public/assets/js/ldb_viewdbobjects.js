var ActionState ={
    None : 0,
    CreateTable : 1,
    CreateLine : 2,
    MoveTable : 3,
    MoveMany : 4,
    SelectLink : 5,
    SelectMany : 6
};

var ViewDbObjects=function(){
    self = this;

    var diagr = $('#diagr-canvas');
    var context = diagr[0].getContext('2d');
    var dbObjects = new DbObjects(context,config);
    var oldPosX=0;
    var oldPosY=0;

    var selBegX=0;
    var selBegY=0;
    var selEndX=0;
    var selEndY=0;

    self.actionState = ActionState.None;

    var begLineObj=null;

    self.clear = function(){
        self.actionState = ActionState.None;
        begLineObj = null;

        delete dbObjects;
        dbObjects = new DbObjects(context,config);

        context.clearRect(0,0,context.canvas.width, context.canvas.height);
    };

    $('#bnt-add-table').click(function(){
        self.actionState = ActionState.CreateTable;
        diagr.css('cursor','crosshair');
    });

    $('#bnt-add-line').click(function(){
        self.actionState = ActionState.CreateLine;
        diagr.css('cursor','crosshair');
    });

    var getOffsetX = function(clientX){
        return Math.round(clientX - diagr.offset().left);
    };

    var getOffsetY = function(clientY){
        return Math.round(clientY - diagr.offset().top);
    };

    diagr.mousedown(function(e){
        if ((self.actionState === ActionState.CreateTable) ||
            (self.actionState === ActionState.CreateLine)){
            return  true;
        }

        var x = getOffsetX(e.clientX);
        var y = getOffsetY(e.clientY);

        if (dbObjects.isSelectedObjects(x,y)){
            oldPosX = x;
            oldPosY = y;
            diagr.css('cursor','move');
            self.actionState = ActionState.MoveMany;

            $(self).trigger('unselectObject',dbObjects.selectedObjects[0]);
        }else if (dbObjects.selectObject(x,y) !== null){
            oldPosX = x;
            oldPosY = y;

            switch (dbObjects.selectedObjects[0].type){
                case ObjectType.Table:
                    self.actionState = ActionState.MoveTable;
                    diagr.css('cursor','move');
                    break;
                case ObjectType.Link:
                    self.actionState = ActionState.SelectLink;
                    break;
                default :
                    self.actionState = ActionState.None;
            }

            $(self).trigger('selectObject',dbObjects.selectedObjects[0]);
        }else{
            selBegX= x;
            selBegY= y;
            self.actionState = ActionState.SelectMany;

            $(self).trigger('unselectObject',dbObjects.selectedObjects[0]);
        }

        dbObjects.drawAll();
    });

    diagr.mousemove(function(e){
        if ((self.actionState === ActionState.MoveTable) ||
            (self.actionState === ActionState.MoveMany))
        {
            var x = getOffsetX(e.clientX);
            var y = getOffsetY(e.clientY);

            var deltaX = x - oldPosX;
            var deltaY = y - oldPosY;

            if ((Math.abs(deltaX) > config.general.moveStep) ||
                (Math.abs(deltaY) > config.general.moveStep)){
                dbObjects.move(deltaX,deltaY);
                oldPosX = x;
                oldPosY = y;
            }
        }else if (self.actionState === ActionState.SelectMany){
            selEndX = getOffsetX(e.clientX);
            selEndY = getOffsetY(e.clientY);

            dbObjects.drawSelectionNet(new Rect(selBegX, selBegY, selEndX - selBegX, selEndY - selBegY));

            dbObjects.drawAll();
        }
    });

    diagr.mouseup(function(e){
        if ((self.actionState === ActionState.MoveTable) ||
            (self.actionState === ActionState.MoveMany))
        {
            diagr.css('cursor','default');
            self.actionState = ActionState.None;
        }else if (self.actionState === ActionState.SelectMany){
            selEndX= getOffsetX(e.clientX);
            selEndY= getOffsetY(e.clientY);

            var x=Math.min(selBegX,selEndX);
            var w=Math.max(selBegX,selEndX)-x;
            var y=Math.min(selEndY,selBegY);
            var h=Math.max(selEndY,selBegY)-y;
            dbObjects.selectObjects(x,y,w,h);

            self.actionState = ActionState.None;

            dbObjects.drawAll();
        }
    });

    diagr.click(function(e){
        var x = getOffsetX(e.clientX);
        var y = getOffsetY(e.clientY);

        if (self.actionState === ActionState.CreateLine){
            if (begLineObj === null){
                begLineObj = dbObjects.getObjectByPos(x,y);
                return true;
            }

            if (begLineObj !== null){
                var endLineObj = dbObjects.getObjectByPos(x,y);
                if (endLineObj !== null) {
                    if (begLineObj.id !== endLineObj.id){
                        diagr.css('cursor','default');

                        dbObjects.addLink(begLineObj,endLineObj);

                        begLineObj=null;
                        self.actionState=ActionState.None;
                    }
                }
            }
        }

        if (self.actionState === ActionState.CreateTable){
            dbObjects.addTable(x,y);
            $(this).css('cursor','default');
            self.actionState=ActionState.None;

            dbObjects.drawAll();
        }
    });

    self.getTables = function(){
        var tables = [];
        for (var i = 0, j = dbObjects.tables.length; i < j; ++i){
            tables.push(dbObjects.tables[i].name);
        }

        return tables;
    };

    self.fieldMove= function (field,positions) {
        dbObjects.fieldMove(field,positions);
        dbObjects.drawAll();
    };

    self.updateField= function (field) {
        dbObjects.updateField(field);
        dbObjects.drawAll();
    };

    self.removeField= function (field) {
        dbObjects.removeField(field);
        dbObjects.drawAll();
    };

    self.addField = function(table,fieldDescr){
        console.log(fieldDescr);
        dbObjects.addFieldToTable(table,{
            name:fieldDescr.name,
            pk:fieldDescr.pk,
            dataType:fieldDescr.dataType,
            dataLen:fieldDescr.dataLen
        });

        dbObjects.drawAll();
    };

    self.renameTable=function(){
        dbObjects.drawAll();
    };

    self.removeTable=function(table){
        dbObjects.removeTable(table);

        dbObjects.drawAll();
    };

    self.upgradeTables = function (tablesDefs) {
        dbObjects.upgradeTables(tablesDefs);

        dbObjects.drawAll();
    };

    self.upgradeLinks = function(linksDefs){
        dbObjects.upgradeLinks(linksDefs);

        dbObjects.drawAll();
    };

    self.upgradeIndexes = function(indexesDefs){
        dbObjects.upgradeIndexes(indexesDefs);
    };

    self.fromObject = function(diagram){
        if (typeof diagram.tables === 'object'){
            for (var i = 0, j = diagram.tables.length; i < j; ++i){
                var tableObj = diagram.tables[i];
                if ((typeof tableObj.x !== 'undefined')
                    || (typeof tableObj.y !== 'undefined')
                    || (typeof tableObj.x !== 'undefined')){

                    var table = dbObjects.addTable(tableObj.x, tableObj.y, tableObj.name);

                    if (typeof tableObj.fields !== 'undefined') {
                        for (var k = 0, l = tableObj.fields.length; k < l; ++k) {
                            dbObjects.addFieldToTable(table, tableObj.fields[k]);
                        }
                    }

                    if (typeof tableObj.indexes !== 'undefined')
                        continue;

                    for (k = 0, l = tableObj.indexes.length; k < l; ++k){
                        var index = tableObj.indexes[k];
                        dbObjects.addIndexToTable(table, index.name, index.type, index.props);
                    }
                }
            }
        }

        if (typeof diagram.links === "object"){
            self.upgradeLinks(diagram.links);
        }

        dbObjects.drawAll();
    };

    self.toObject = function(){
        var tables = [];
        var links = [];

        for (var i = 0, j = dbObjects.tables.length; i < j; ++i){
            tables.push(dbObjects.tables[i].toObject());
        }

        for (i = 0, j = dbObjects.links.length; i < j; ++i){
            links.push(dbObjects.links[i].toObject());
        }

        return {
            tables: tables,
            links: links
        }
    };

    self.redraw = function(){
        dbObjects.drawAll();
    };
};
