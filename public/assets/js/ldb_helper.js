var ItemListExistsTables = function (name,check,inDiagram,show) {
    return {
        name: name,
        check: check,
        inDiagra: inDiagram,
        show: show
    }
};

var ajaxPostJson = function(url,data,params){
    var data ={
        url : url,
        data : data,
        type : 'POST',
        dataType : 'json',
        async : false
    };

    return $.ajax(data);
};

var each = function(array, action, context){
    if ((typeof array === 'undefined') || (typeof action === 'undefined'))
        return;

    if (!(array instanceof Array))
        return;

    if (typeof context !== 'undefined'){
        for (var i = 0, j = array.length; i < j; ++i){
            action.call(context,array[i],i);
        }
    }else{
        for (i = 0, j = array.length; i < j; ++i){
            action(array[i],i);
        }
    }
};

var ObjectType ={
    Table : 0,
    Link : 1
};

var Pos = function(x,y){
    return {
        x:x,
        y:y
    }
};

var Rect = function(x, y, width, height){
    return {
        x:x,
        y:y,
        width:width,
        height:height
    }
};