var config ={
    general:{
        moveStep:4,
        selectionNetColor:"#000000"
    },
    font:{
        size:15,
        name: 'Arial'
    },
    field:{
        nameColor:"#000000",
        typeColor:"#1625ff",
        pkColor:"#ff1625"
    },
    table:{
        textPadding:3,
        minWidth:100,
        borderColor: "#b3b3b3",
        backgroundColor: "#d9edf7",
        textColor: "#000000",
        selSideLen:5,
        selColor:"#444444",
        borderRadius:5
    },
    link:{
        color: "#000000"
    }
};

$(document).ready(function(){
    function loadTemplates(requiries){
        if (!(requiries instanceof Array))
            return;

        for(var i=0;i<requiries.length;i++){
            if (!(requiries[i] instanceof Array))
                continue;

            if (requiries[i].length != 2)
                continue;

            var selector = requiries[i][0];
            var url = requiries[i][1];

            $.ajax({
                url:url,
                async:false,
                dataType:'text',
                success:function(data){
                    $(selector).append(data);
                }
            })
        }
    }

    function loadScripts(requiries){
        if (!(requiries instanceof Array))
            return;

        for(var i=0;i<requiries.length;i++){
            var url = requiries[i];

            $.ajax({
                url:url,
                async:false,
                dataType:'script',
                success:function(data){
                    $('body').append($('script').text(data));
                }
            })
        }
    }

    loadTemplates([
        ['body','/assets/templates/authform.html'],
        ['body','/assets/templates/showdiagrams.html'],
        ['body','/assets/templates/showtables.html'],
        ['body','/assets/templates/newdiagr.html'],
        ['body','/assets/templates/boxes.html']
    ]);

    loadScripts([
        '/assets/js/ldb_helper.js',
        '/assets/js/ldb_boxes.js',
        '/assets/js/ldb_link.js',
        '/assets/js/ldb_field.js',
        '/assets/js/ldb_table.js',
        '/assets/js/ldb_index.js',
        '/assets/js/ldb_objects.js',
        '/assets/js/ldb_viewdbobjects.js',
        '/assets/js/ldb_showtables.js',
        '/assets/js/ldb_appview.js',
        '/assets/js/ldb_authform.js',
        '/assets/js/ldb_showdiagrams.js',
        '/assets/js/ldb_newdiagr.js'
    ]);

    var app = new AppView();
    ko.applyBindings(app);

    $("#diagr-canvas").keydown(function(event){
        switch (event.which){
            case (46):
                app.removeTable();
                break;
            default:
                break;
        }
    });

    $(document).keydown(function(event){
        if ((event.which == 83) && event.ctrlKey && (!event.shiftKey)){
            app.saveDiagram();
            return false;
        }

        if ((event.which == 78) && event.ctrlKey && (!event.shiftKey)){
            app.newDiagram();
            return false;
        }

        if ((event.which == 79) && event.ctrlKey && (!event.shiftKey)){
            app.showDiagrams();
            return false;
        }
    });

    app.openDiagram(1);
});