var Index = function(table, name, type, props){
    return {
        table : table,
        name : name,
        type : type,
        props : props,

        toObject : function(){
            var props = [];

            for (var i = 0, j = this.props.length; i < j; ++i){
                var prop = this.props[i];
                props.push({
                    fieldName : prop.field.name,
                    pos : prop.pos,
                    uniq : prop.uniq
                });
            }

            return {
                tableName : this.table.name,
                name : this.name,
                type : this.type,
                props : props
            }
        }
    }
};