var dbdNewDiagrForm = (function (){
    var modalSelector = '#modal-new-diagr';

    var form = $(modalSelector);
    var header = form.find(modalSelector+'-title');
    var btnSave = form.find(modalSelector+'-btn-save');
    var btnTest = form.find(modalSelector+'-btn-test');
    var btnClose = form.find(modalSelector+'-btn-close');
    var btnCancel = form.find(modalSelector+'-btn-cancel');
    var title = form.find(modalSelector+'-form-input-title');
    var alert = form.find(modalSelector+'-alert');

    var adapter = form.find(modalSelector+'-form-input-adapter');
    var host = form.find(modalSelector+'-form-input-host');
    var port = form.find(modalSelector+'-form-input-port');
    var dbname = form.find(modalSelector+'-form-input-dbname');
    var user = form.find(modalSelector+'-form-input-user');
    var pass = form.find(modalSelector+'-form-input-pass');

    var resultClick = false;
    var response = null;

    form.modal({
        show:false
    });

    form.on('shown.bs.modal', function(e){
        title.focus();
    });

    var getResponseData = function(){
        return {title:title.val(),
            adapter:adapter.val(),
            host:host.val(),
            port:port.val(),
            dbname:dbname.val(),
            user:user.val(),
            pass:pass.val()
        };
    };

    var btnSaveClick=function(){
        response(getResponseData());

        form.modal('hide');
    };

    var btnCloseClick=function(){
        response(null);
        form.modal('hide');
    };

    var showAlert = function(message,type){
        alert.empty();
        alert.attr('class','alert alert-'+type);
        alert.html(message);
    };

    var btnTestClick = function(){
        $.ajax({
            url:'/json/test_conn',
            data:getResponseData(),
            dataType:'json',
            async:false,
            error: function(jqXHR,textStatus,errorThrown){
                if (typeof jqXHR.responseJSON === "undefined") {
                    showAlert(jqXHR.statusText,'danger');
                } else {
                    showAlert(jqXHR.responseJSON.error,'danger');
                }
            },
            success:function(){
                showAlert('Connection: OK','success');
            }
        })
    }

    btnSave.click(btnSaveClick);
    btnClose.click(btnCloseClick);
    btnCancel.click(btnCloseClick);
    btnTest.click(btnTestClick);

    var showModal=function(titleModal,rescallback){
        response=rescallback;

        alert.empty()
            .removeClass();

        header.empty()
            .append('<span><b>'+titleModal+'</b></span>');

        title.val('');
        host.val('')
        dbname.val('')
        user.val('');
        pass.val('');

        form.modal('show');
    };

    return {
        show : function(success,error){
            showModal('Create new diagram',function(result){
                if (result){
                    $.ajax({
                        url:'/json/create_diagram',
                        data:result,
                        dataType:'json',
                        async:false,
                        error: error,
                        success:success
                    })
                }
            })
        }
    }
})();

