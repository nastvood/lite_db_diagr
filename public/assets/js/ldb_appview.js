var AppView = function(){
    'use strict';

    var self = this;
    var context = $('#diagr-canvas')[0].getContext('2d');

    var viewDbObjects = new ViewDbObjects();

    $(viewDbObjects).on('selectObject',function(e,selectedObject){
        self.currentObject(selectedObject);
    });

    $(viewDbObjects).on('unselectObject',function(){
        self.currentObject(null);
    });

    function resize(event){
        context.canvas.width = $(window).width();
        context.canvas.height = 5000;
        $("#diagr-container").height($(window).height() - 70);
        $("#left-panel").height($(window).height() - 70);
    }

    resize();

    $(window).resize(function(){
        resize();
        viewDbObjects.redraw();
    });

    self.addFunction = function(name, action){
        if (self.hasOwnProperty(name))
            throw new Error('[addFunction]: function ' + name + ' already exist');
        else
            self[name] = action;
    };

    var defaultCallbackErr = function(jqXHR, textStatus, errorThrown){
        if (typeof jqXHR.responseJSON === 'undefined') {
            dbdMessages.showDanger('Error',jqXHR.statusText);
        } else {
            dbdMessages.showDanger('Error',jqXHR.responseJSON.error);
        }
    };

    self.currentObject = ko.observable(null);
    self.dataTypes = ko.observableArray(['INT','CHAR','VARCAHR']);
    self.newField = ko.observable(null);
    self.adapters = ko.observable(null);
    self.diagrams = ko.observableArray([]);
    self.diagram = ko.observable(null);
    self.existsTables = ko.observableArray([]);

    self.settingShowFields = ko.observable(false);
    self.toggleShowSettingShowFields = function(e){
        self.settingShowFields(!self.settingShowFields.peek());
    };

    self.settingShowTable = ko.observable(false);
    self.toggleShowSettingShowTable = function(){
        self.settingShowTable(!self.settingShowTable.peek());
    };

    self.settingShowIndex = ko.observable(false);
    self.toggleShowSettingShowIndex = function(){
        self.settingShowIndex(!self.settingShowIndex.peek());
    };

    self.settingShowFieldDescr = function(field){
        var selector = '#field_' + field.name;
        $(selector).toggle();
    };

    $.ajax({
        url:'/json/adapters',
        dataType:'json',
        error: defaultCallbackErr,
        success:self.adapters,
        async:false
    });

    self.setDataTypeLabel = function(data,event,field){
        var btn = $(event.target).parent().parent().siblings()[0];
        $(btn).children("span.drop-down-data-type").text(data);

        field.dataType=data;
    };

    self.fieldUp=function(field){
        viewDbObjects.fieldMove(field,-1);
        self.currentObject.valueHasMutated();
    };

    self.fieldDown=function(field){
        viewDbObjects.fieldMove(field,1);
        self.currentObject.valueHasMutated();
    };

    self.updateField= function (field) {
        viewDbObjects.updateField(field);
        self.currentObject.valueHasMutated();
    };

    self.removeField = function(field){
        viewDbObjects.removeField(field);
        self.currentObject.valueHasMutated();
    };

    self.showDlgAddField = function(table){
        self.newField({
            table:table,
            name:"Field"+table.fields.length,
            pk:false,
            dataType:"",
            dataLen:""
        });

        $('#form-new-field').modal('show');
    };

    self.addField = function(newField){
        $('#form-new-field').modal('hide');
        viewDbObjects.addField(newField.table,newField);
        self.currentObject.valueHasMutated();
    };

    self.renameTable = function(){
        viewDbObjects.renameTable();
    };

    self.removeTable = function(table){
        if (typeof table === 'undefined'){
            if (self.currentObject() != null){
                viewDbObjects.removeTable(self.currentObject());
                self.currentObject(null);
            }
        }else {
            self.currentObject(null);
            viewDbObjects.removeTable(table);
        }
    };

    self.signup = function(){
        dbdAuthForm.signup(function(){
            document.location.href = "/";
        },defaultCallbackErr);
    };

    self.login = function(){
        dbdAuthForm.login(function(){
            document.location.href = "/";
        },defaultCallbackErr);
    };

    self.logout = function(){
        dbdAuthForm.logout(function(){
            document.location.href = "/";
        },defaultCallbackErr);
    };

    self.newDiagram = function(){
        dbdNewDiagrForm.show(function(){
            //document.location.href = "/";
        },defaultCallbackErr);
    };

    self.showDiagrams = function(){
        dbdModalShowDiagrs.show(self.diagrams,defaultCallbackErr);
    };

    self.openDiagram = function(diagram){
        var docId = (typeof diagram === 'object' ? diagram.id : diagram);
        dbdModalShowDiagrs.open(docId,
            function (data) {
                self.closeDiagram();
                self.diagram(data);
            },
            self.diagram,
            function(jqXHR,textStatus,errorThrown){
                self.diagram(null);
                defaultCallbackErr(jqXHR,textStatus,errorThrown);
        });

        if (self.diagram.peek())
            viewDbObjects.fromObject(self.diagram.peek().diagram);
    };

    self.showListExistsTables = function(){
        dbdModalShowTables.show(self.diagram().id, self.existsTables,
            viewDbObjects.getTables(), defaultCallbackErr);
    };

    self.upgradeTables = function(){
        var checkedTables =  [];
        var tables = self.existsTables.peek();
        for (var i = 0, j = tables.length; i < j; ++i){
            if (tables[i].check){
                checkedTables.push(tables[i].name);
            }
        }

        dbdModalShowTables.getDefs(
            self.diagram.peek().id,
            checkedTables,
            viewDbObjects.upgradeTables,
            viewDbObjects.upgradeIndexes,
            viewDbObjects.upgradeLinks
        );
    };

    self.saveDiagram = function(){
        if (!self.diagram)
            return;

        var diagram = JSON.stringify(viewDbObjects.toObject());
        var title = self.diagram().title;
        var connect = JSON.stringify(self.diagram().connect);
        var id = self.diagram().id;

        $.ajax({
            url:'/json/save',
            data: {id:id,title:title,connect:connect,diagram:diagram},
            type:'POST',
            dataType:'json',
            async:false,
            error: defaultCallbackErr
        });
    };

    self.closeDiagram = function(){
        if (self.diagram.peek()) {
            self.diagram(null);
            viewDbObjects.clear();
        }
    };

    var dbdModalShowTables = new DbdModalShowTables(self);
    var dbdModalShowDiagrs = new DbdModalShowDiagrs(self);
};


