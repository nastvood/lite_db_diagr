var Link = function(context, field, fieldRef, name){
    var self = this;
    var path = [];

    field.table.links.push(self);
    fieldRef.table.links.push(self);

    self.id=0;
    self.name=name;
    self.name = (typeof name == "undefined" ? "Link" : name);
    self.type = ObjectType.Link;
    self.selected = false;
    self.field = field;
    self.fieldRef = fieldRef;

    var drawTail = function(x, y, dir){
        var len = 5 * dir;
        context.moveTo(x + len, y);
        context.lineTo(x + dir, y + 5);
        context.moveTo(x + len, y);
        context.lineTo(x + dir, y - 5);
    };

    var drawPath = function(dir){
        var pathLen = path.length;

        if (!pathLen){
            return;
        }

        var pos = path[0];
        context.moveTo(pos.x, pos.y);
        context.arc(pos.x, pos.y, 2, 0, 2 * Math.PI, false);

        for(var i = 1; i < pathLen; ++i){
            pos = path[i];
            context.lineTo(pos.x, pos.y);
        }

        if (pathLen>1){
            drawTail(path[pathLen - 1].x, path[pathLen - 1].y,dir);
        }

        if (self.selected) {
            context.setLineDash([2]);
        }

        context.stroke();

        if (self.selected){
            context.setLineDash([]);
        }
    };

    self.setId=function(id){
        self.id=id;

        if (self.name === "Link"){
            self.name += self.id;
        }
    };

    self.draw = function(){
        context.strokeStyle = config.link.color;
        context.beginPath();

        path=[];

        var distX = fieldRef.x + fieldRef.width - field.x;
        var halfDistX = Math.round(distX / 2);
        var halfObjSrcHeight = Math.round(field.height / 2);
        var halfObjDstHeight = Math.round(fieldRef.height / 2);
        var dir=0;

        if (fieldRef.x + fieldRef.width <= field.x){
            //слева
            path.push(new Pos(fieldRef.x + fieldRef.width, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x + fieldRef.width - halfDistX, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x + fieldRef.width - halfDistX, field.y + halfObjSrcHeight));
            path.push(new Pos(field.x, field.y + halfObjSrcHeight));
            dir = -1;
        }else if ((fieldRef.x < field.x) && (field.x < fieldRef.x + fieldRef.width)){
            path.push(new Pos(fieldRef.x, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x - 20,fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x - 20,field.y + halfObjSrcHeight));
            path.push(new Pos(field.x, field.y + halfObjSrcHeight));
            dir = -1;
        }else if (field.x + field.width <= fieldRef.x){
            //справа
            distX = fieldRef.x - field.x - field.width;
            halfDistX = Math.round(distX / 2);

            path.push(new Pos(fieldRef.x, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x - halfDistX,fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x - halfDistX,field.y + halfObjSrcHeight));
            path.push(new Pos(field.x + field.width,field.y + halfObjSrcHeight));
            dir = 1;
        }else{
            path.push(new Pos(fieldRef.x + fieldRef.width, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x + fieldRef.width + 20, fieldRef.y + halfObjDstHeight));
            path.push(new Pos(fieldRef.x + fieldRef.width + 20, field.y + halfObjSrcHeight));
            path.push(new Pos(field.x + field.width, field.y + halfObjSrcHeight));
            dir = 1;
        }

        drawPath(dir);
    };

    self.toObject = function(){
        return {
            id:self.id,
            name:self.name,
            fieldName:field.name,
            tableName:field.table.name,
            refFieldName:fieldRef.name,
            refTableName:fieldRef.table.name
        };
    };

    self.isPointInPath = function(x,y){
        var pathLen = path.length;
        var lineWidth=1;
        for (var i=0;i<(pathLen-1);i++){
            var pos1 = path[i];
            var pos2= path[i+1];

            if (pos1.y === pos2.y){
                var x1 = Math.min(pos1.x,pos2.x);
                var x2 = Math.max(pos1.x,pos2.x);
                if ((x1<=x) && (x<=x2)){
                    if (((pos1.y-lineWidth)<=y) && (y<=(pos1.y+lineWidth))){
                        return true;
                    }
                }
            }

            if (pos1.x === pos2.x){
                var y1 = Math.min(pos1.y,pos2.y);
                var y2 = Math.max(pos1.y,pos2.y);
                if ((y1<=y) && (y<=y2)){
                    if (((pos1.x-lineWidth)<=x) && (x<=(pos1.x+lineWidth))){
                        return true;
                    }
                }
            }
        }

        return false;
    };

    self.removeLinkFromTables = function(){
        field.table.removeLink(this);
        fieldRef.table.removeLink(this);
    };

    self.isBelongs = function(checkField){
        if ((field.id === checkField.id)
            || (fieldRef.id === checkField.id)){
            return true;
        }

        return false;
    };
};