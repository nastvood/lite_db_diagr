var DbObjects=function(context,config){
    var self=this;

    var selectionNet = null;
    var oldClearRect = null;

    self.tables=[];
    self.links=[];
    self.idCounter=0;
    self.selectedObjects=[];

    var calcClearRect = function(){
        var x = 0, y = 0, w = 0, h = 0;
        for(var i = 0; i < self.tables.length; i++){
            x = Math.min(x, self.tables[i].x);
            y = Math.min(y, self.tables[i].y);
            w = Math.max(w, self.tables[i].x + self.tables[i].width + config.table.selSideLen * 2);
            h = Math.max(h, self.tables[i].y + self.tables[i].height + config.table.selSideLen * 2 );
        }

        return new Rect(x, y, w, h);
    };

    self.addTable=function(x,y,name){
        self.idCounter++;

        var tableName = name || ("Table"+self.idCounter);
        var table=new Table(context,config,x,y,tableName);
        table.setId(self.idCounter);

        self.tables.push(table);

        return table;
    };

    self.addLink=function(fieldDst,fieldSrc,name){
        self.idCounter++;

        var link=new Link(context,fieldDst,fieldSrc,name);
        link.setId(self.idCounter);

        self.links.push(link);

        return link;
    };

    self.addFieldToTable = function(objTbl, fieldDescr){
        return objTbl.addField(context, config, fieldDescr);
    };

    self.addIndexToTable = function(table,name,type,props){
        return table.addIndex(name,type,props);
    };

    self.drawSelectionNet = function(rect){
        selectionNet = rect;
    };

    self.drawAll = function(action){
        var clearRect = calcClearRect();

        if (typeof action !== 'undefined'){
            action();

            var rectAfterAction = calcClearRect();

            clearRect.x = Math.min(clearRect.x,rectAfterAction.x);
            clearRect.y = Math.min(clearRect.y,rectAfterAction.y);
            clearRect.width = Math.max(clearRect.width,rectAfterAction.width);
            clearRect.height = Math.max(clearRect.height,rectAfterAction.height);
        }

        if (selectionNet){
            clearRect.x = Math.min(clearRect.x,selectionNet.x);
            clearRect.y = Math.min(clearRect.y,selectionNet.y);
            clearRect.width = Math.max(clearRect.width,selectionNet.width + selectionNet.x + 5);
            clearRect.height = Math.max(clearRect.height,selectionNet.height + selectionNet.y + 5);
        }

        if (oldClearRect != null){
            clearRect.x = Math.min(clearRect.x,oldClearRect.x);
            clearRect.y = Math.min(clearRect.y,oldClearRect.y);
            clearRect.width = Math.max(clearRect.width,oldClearRect.width);
            clearRect.height = Math.max(clearRect.height,oldClearRect.height);
        }

        context.clearRect(clearRect.x, clearRect.y, clearRect.width, clearRect.height);

        oldClearRect = clearRect;

        for(i = 0; i < self.tables.length; i++){
            self.tables[i].draw();
        }

        for(i = 0; i < self.links.length; i++){
            self.links[i].draw();
        }

        if (selectionNet){
            context.fillStyle = config.general.selectionNetColor;
            context.rect(selectionNet.x, selectionNet.y, selectionNet.width, selectionNet.height);
            context.stroke();

            selectionNet = null;
        }
    };

    self.move=function(offsetX,offsetY){
        self.drawAll(function(){
            for (var i=0;i<self.selectedObjects.length;++i){
                self.selectedObjects[i].x+=offsetX;
                self.selectedObjects[i].y+=offsetY;
            }
        });
    };

    self.getObjectByPos = function(x, y){
        for (var i = 0, j = self.tables.length; i < j; ++i){
            var obj=self.tables[i];
            if ((obj.x <= x)  && (x <= (obj.width + obj.x)) &&
                (obj.y <= y) && (y <= (obj.height + obj.y))) {
                return obj;
            }
        }

        for (i = 0, j = self.links.length; i < j; ++i){
            if (self.links[i].isPointInPath(x, y)){
                return self.links[i];
            }
        }

        return null;
    };

    var clearSelected = function (){
        for (var i = 0, j = self.selectedObjects.length; i < j; ++i){
            self.selectedObjects[i].selected = false;
        }
        self.selectedObjects.length = 0;
    };

    self.selectObject=function(x,y){
        clearSelected();

        var obj = self.getObjectByPos(x,y);
        if (obj !== null){
            obj.selected=true;
            self.selectedObjects.push(obj);
        }else{
            self.selectedObjects.length=0;
        }

        return obj;
    };

    self.isSelectedObjects=function(x,y){
        if (self.selectedObjects.length <= 1){
            return false;
        }

        for (var i = 0, j = self.selectedObjects.length; i < j; ++i){
            var obj=self.selectedObjects[i];
            if ((obj.x<=x)  && (x<=(obj.width+obj.x)) &&
                (obj.y<=y) && (y<=(obj.height+obj.y))) {
                return true;
            }
        }

        return false;
    };

    self.selectObjects = function(x,y,w,h){
        clearSelected();

        for (var i = 0, j = self.tables.length; i < j; ++i){
            var obj=self.tables[i];
            if ((((obj.x<x) && (x<obj.x+obj.width)) ||
                ((obj.x<x+w) && (x+w<obj.x+obj.width)) ||
                ((x<obj.x) && (x+w>obj.x+obj.width)) ||
                ((x>obj.x) && (x+w<obj.x+obj.width))
                ) &&
                (((obj.y<y) && (y<obj.y+obj.height))||
                    ((obj.y<y+h) && (y+h<obj.y+obj.height))||
                    ((y<obj.y) && (y+h>obj.y+obj.height))||
                    ((y>obj.y) && (y+h<obj.y+obj.height))
                    ))
            {
                obj.selected=true;
                self.selectedObjects.push(obj);
            }
        }
    };

    self.fieldMove= function (field,positions) {
        var table=field.table;
        var index=table.getFieldIndex(field);
        var newIndex=index+positions;

        if ((index >= 0) && (newIndex >= 0)){
            var fieldReplaceable=table.fields[newIndex];
            table.fields[newIndex]=field;
            table.fields[index]=fieldReplaceable;
        }
    };

    self.updateField = function(field){
        var table=field.table;
        var index=table.getFieldIndex(field);

        if (index >= 0){
            table.fields[index]=field;
        }
    };

    self.removeLink = function(link){
        for (var i = 0, j = self.links.length; i < j; ++i){
            if (self.links.indexOf(link) !== -1){
                self.links[i].removeLinkFromTables();
                self.links.splice(i,1);
            }
        }
    };

    self.removeField = function(field){
        var table=field.table;
        var index=table.getFieldIndex(field);

        if (index>-1){
            for (var i = 0, j = table.links.length; i < j; ++i){
                if (table.links[i].isBelongs(field)){
                    self.removeLink(table.links[i]);
                    break;
                }
            }
            table.fields.splice(index,1);
        }
    };

    self.removeTable = function(table){
        if (typeof table === 'string') {
            var tableObj  = self.getTableByName(table);
            if (tableObj){
                table = tableObj;
            }

            return;
        }

        if (typeof table === 'object') {
            var index = self.tables.indexOf(table);
            if (index !== -1) {
                var links = table.links.slice(0);
                for (var i = 0, j = links.length; i < j; ++i) {
                    self.removeLink(links[i]);
                }
                self.tables.splice(index, 1);
            }
        }
    };

    self.upgradeTables = function(tablesDefs){
        for (var i = 0, j = tablesDefs.length; i < j; ++i) {
            var tableDef = tablesDefs[i];
            var table = self.getTableByName(tableDef.name);
            if (table) {
            } else {
                table = self.addTable(5, 5, tableDef.name);
                for (var k = 0, l = tableDef.fields.length; k < l; ++k) {
                    self.addFieldToTable(table, tableDef.fields[k]);
                }
            }
        }
    };

    self.upgradeIndexes = function (indexesDefs) {
        for (var i = 0, j = indexesDefs.length; i < j; ++i) {
            var tableIndexesDef = indexesDefs[i];
            var table = self.getTableByName(tableIndexesDef.name);

            if (table){
                for (var k = 0, l = tableIndexesDef.indexes.length; k < l; ++k) {
                    var index = tableIndexesDef.indexes[k];
                    table.addIndex(index.name, index.type, index.props);
                }
            }
        }
    };

    self.getTableByName = function(tableName){
        for (var i = 0, j = self.tables.length; i < j; ++i){
            if (tableName === self.tables[i].name)
                return self.tables[i];
        }

        return null;
    };

    self.getFieldByName = function(tableName,fieldName){
        for (var i = 0, j = self.tables.length; i < j; ++i){
            if (tableName === self.tables[i].name)
                return self.tables[i].getFieldByName(fieldName);
        }

        return null;
    };

    self.upgradeLinks = function(linksDefs){
        for (var i = 0,j = linksDefs.length; i < j; ++i){
            var link = linksDefs[i];

            var field = self.getFieldByName(link.tableName, link.fieldName);
            var refField = self.getFieldByName(link.refTableName, link.refFieldName);

            if ((field) && (refField)){
                var foundLink = self.getLinkByFieldsName(field.name,refField.name);
                if (!foundLink){
                    self.addLink(field, refField, link.name);
                }
            }
        }
    };

    self.getLinkByFieldsName = function(field,fieldRef){
        for (var i = 0, j = self.links.length; i < j; ++i){
            if ((field === self.links[i].field.name) && (fieldRef === self.links[i].fieldRef.name))
                return self.links[i];
        }

        return null;
    }
};

