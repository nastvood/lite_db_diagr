var Table=function(context,config,x,y,name){
    var self = this;

    self.id = 0;
    self.name = name;
    self.links = [];
    self.fields = [];
    self.indexes = [];

    self.width = config.table.minWidth;
    self.height = 0;
    self.x = x;
    self.y = y;
    self.selected = false;
    self.type = ObjectType.Table;

    self.setId = function(id){
        self.id = id;
    };

    self.addField = function(context,config,fieldDescr){
        var field = new Field(context,config,fieldDescr);
        self.fields.push(field);
        field.table = self;

        return field;
    };

    self.getFieldIndex = function(field){
        for(var i = 0, j = self.fields.length; i<j; ++i){
            if (self.fields[i].name === field.name){
                return i;
            }
        }

        return -1;
    };

    self.getFieldByName = function(fieldName){
        for(var i = 0, j = self.fields.length; i<j; ++i){
            if (fieldName === self.fields[i].name)
                return self.fields[i];
        }

        return null;
    };

    self.addIndex = function(name,type,indexProps){
        var fields = [];

        for (var i = 0, j = indexProps.length; i < j; ++i){
            var field = self.getFieldByName(indexProps[i].fieldName);
            if (field){
                fields.push({
                    field : field,
                    pos : indexProps[i].pos,
                    uniq : indexProps[i].uniq
                });
            }
        }

        var index = new Index(this,name,type,fields);

        self.indexes.push(index);
    };

    var drawTitle = function(){
        context.font = 'bold ' + config.font.size + 'px ' + config.font.name;
        var metricsTableName = context.measureText(self.name);

        context.fillStyle = config.table.textColor;
        context.fillText(self.name,
            self.x + config.table.textPadding + self.width / 2 - metricsTableName.width / 2,
            self.y + config.table.textPadding + config.font.size
        );
    };

    self.draw = function(){
        var widthFieldName = 0;
        var widthFieldType = 0;
        var widthFieldPK = 0;
        for(var i = 0, j = self.fields.length; i<j; ++i){
            var minWidths = self.fields[i].getMinWidth();

            widthFieldName = Math.max(minWidths[0], widthFieldName);
            widthFieldType = Math.max(minWidths[1], widthFieldType);
            widthFieldPK = Math.max(minWidths[2], widthFieldPK);
        }

        self.width = Math.max(self.width, widthFieldName + widthFieldType + widthFieldPK);
        var fieldHeight = config.table.textPadding * 3 + config.font.size;
        self.height = fieldHeight;

        self.height = fieldHeight * (self.fields.length + 1) + config.table.textPadding * 2;

        context.fillStyle = config.table.backgroundColor;
        context.fillRect(self.x, self.y, self.width, self.height);

        context.strokeStyle = config.table.borderColor;
        var radius = config.table.borderRadius;
        context.beginPath();
        context.moveTo(self.x, self.y + radius);
        context.arc(self.x + radius, self.y + radius, radius, Math.PI, 1.5 * Math.PI);
        context.lineTo(self.x + self.width - radius, self.y);
        context.arc(self.x + self.width - radius, self.y + radius, radius, 1.5 * Math.PI, 0);
        context.lineTo(self.x + self.width, self.y + self.height - radius);
        context.arc(self.x + self.width - radius, self.y + self.height - radius, radius, 0, 0.5 * Math.PI);
        context.lineTo(self.x + radius, self.y + self.height);
        context.arc(self.x + radius, self.y + self.height - radius, radius, 0.5 * Math.PI, Math.PI );
        context.closePath();

        if (self.fields.length){
            context.moveTo(self.x, self.y + config.table.textPadding * 3 + config.font.size);
            context.lineTo(self.x + self.width,self.y + config.table.textPadding * 3 + config.font.size);
            context.fillStyle = "#000000";
        }

        if (self.selected === true){
            var selLen = config.table.selSideLen;

            context.strokeStyle = config.table.selColor;
            context.rect(self.x - selLen + 1, self.y - selLen + 1, selLen, selLen);
            context.rect(self.x + self.width - 1, self.y - selLen + 1, selLen, selLen);
            context.rect(self.x - selLen + 1, self.y + self.height - 1, selLen, selLen);
            context.rect(self.x + self.width - 1, self.y + self.height - 1, selLen, selLen);
        }

        drawTitle();

        context.font = 'normal ' + config.font.size + 'px ' + config.font.name;
        for(i = 0, j = self.fields.length; i<j; ++i){
            self.fields[i].draw(self.x, self.y + fieldHeight*(i+2),
                widthFieldName, widthFieldType, widthFieldPK, fieldHeight);
        }

        context.stroke();
    };



    self.removeLink = function(link){
        for(var i = 0, j = self.links.length; i < j; ++i){
            if (self.links.indexOf(link) !== -1){
                self.links.splice(i,1);
            }
        }
    };

    self.toObject = function(){
        var fields = [];
        for(var i = 0, j = self.fields.length; i < j; ++i){
            fields.push(self.fields[i].toObject());
        }

        var indexes = [];
        for(i = 0, j = self.indexes.length; i < j; ++i){
            indexes.push(self.indexes[i].toObject());
        }

        return {
            fields:fields,
            id:self.id,
            name:self.name,
            x:self.x,
            y:self.y,
            indexes : indexes
        }
    };
};
