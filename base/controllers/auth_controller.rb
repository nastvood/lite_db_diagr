class Auth < E
  map 'auth'
  format :json

  def signup
    email = params[:email]
    pass = params[:pass]

    halt 500,'wat?' if email.nil? || pass.nil?

    user = Users.new(email: email,pass: pass)
    unless user.valid?
      halt 500, {:error => AppHelpers.get_error_s(user.errors)}.to_json
    end

    if user.save
      session[:user] = user.email
      session[:user_id] = user.id_user
    else
      halt 500, {:error => AppHelpers.get_error_s(user.errors)}.to_json
    end
  end

  def login
    email = params[:email]
    pass = params[:pass]

    halt 500,'wat?' if email.nil? || pass.nil?

    user = Users.first(email:email,pass: pass)
    if user.nil?
      halt 500, {:error => 'User not found'}.to_json
    else
      session[:user] = user.email
      session[:user_id] = user.id_user
    end
  end

  def logout
    session.clear
  end
end