class Json  < E
  map 'json'
  content_type '.json'

  def get_user
    AppHelpers.get_user session
  end

  def adapters
    SequelHelper::Adapters::to_json
  end

  def test_conn
    adapter = params[:adapter]
    host = params[:host]
    dbname = params[:dbname]
    port = params[:port]
    user = params[:user]
    pass = params[:pass]

    raise 'Adapter is empty' if adapter.nil?

    begin
      db = Sequel.connect :adapter=>SequelHelper::Adapters::to_s(adapter),
                           :host=>host,:port=>port,:database=>dbname,
                           :username=>user,:password=>pass,:test => true

      db.disconnect

      true
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def create_diagram
    begin
      user_id,_ = get_user

      title = params[:title]
      connect = {:adapter => params[:adapter],
        :host=>params[:host],
        :port=>params[:port],
        :dbname=>params[:dbname],
        :user=>params[:user],
        :pass=>params[:pass]
      }

      doc = Docs.new(title: title,connect: connect.to_json, user_id: user_id);

      raise AppHelpers.get_error_s(doc.errors) unless doc.valid?
      raise AppHelpers.get_error_s(doc.errors) unless doc.save

      {:id => doc.user_id}.to_json
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def remove_diagram
    begin
      user_id,_ = get_user

      halt 500, {:error => 'Diagram not found'}.to_json if params[:id].nil?

      doc = Docs.with_pk!(params[:id].to_i)
      doc.delete

      {}.to_json
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def diagrams
    user_id,_ = get_user

    Docs.all.map{|doc| {
        :id => doc.id_doc,
        :title => doc.title,
        :created_at => doc.created_at,
        :updated_at => doc.updated_at
    }}.to_json
  end

  def diagram
    begin
      user_id,_ = get_user

      raise 'Diagram not found' if params[:id].nil?
      id = params[:id].to_i

      doc = Docs.with_pk!(id)

      {
        :id => id,
        :title => doc.title,
        :connect => JSON.parse(doc.connect.nil? ? '{}' : doc.connect),
        :diagram => JSON.parse(doc.diagram.nil? ? '{}' : doc.diagram)
      }.to_json
    rescue Sequel::Error => e
      halt 500, {:error => 'Doesn\'t exist diagram'}.to_json
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def tables
    begin
      user_id,_ = get_user

      raise 'Diagram not found' if params[:id].nil?

      doc = Docs.with_pk!(params[:id].to_i)

      SequelHelper.connection(JSON.parse(doc.connect)) do |db|
         db.tables.to_json
      end
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def tables_defs
    begin
      user_id,_ = get_user

      raise 'Diagram not found' if params[:id].nil? || params[:tables].nil?

      id = params[:id].to_i
      tables = params[:tables]

      tables = [tables] unless tables.kind_of?(Array)

      doc = Docs.with_pk!(id)
      SequelHelper.connection(JSON.parse(doc.connect)) do |db|
        tables.inject([]){|defs,table|
          columns = db.schema(table.to_sym).inject([]){|columns,column|
            columns << {
                :name => column[0].to_s,
                :default => column[1][:default],
                :pk => column[1][:primary_key],
                :allowNull => column[1][:allow_null],
                :dataType => column[1][:db_type][/[a-z]*/],
                :dataLen => (column[1][:db_type][/\(\d+\)/].nil? ? "" : column[1][:db_type][/\(\d+\)/][/\d+/])
            }
          }

          defs << {:name => table, :fields => columns}
        }.to_json
      end
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def save
    begin
      user_id,_ = get_user

      id = params[:id]
      title = params[:title]
      connect = params[:connect]
      diagram = params[:diagram]

      raise 'Diagram not found' if id.nil?

      doc = Docs.with_pk!(id)

      doc.title = title
      doc.diagram = diagram
      doc.connect = connect

      raise AppHelpers.get_error_s(doc.errors) unless doc.valid?
      raise AppHelpers.get_error_s(doc.errors) unless doc.save

      {:id => doc.id}.to_json
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def links
    begin
      user_id,_ = get_user

      id = params[:id]
      tables = params[:tables]

      raise 'Diagram not found' if tables.nil? || id.nil?

      tables = [tables] unless tables.kind_of?(Array)

      doc = Docs.with_pk!(id)

      SequelHelper.connection(JSON.parse(doc.connect)) do |db|
        if tables.size > 0
          SequelHelper.foreign_keys(db,tables).to_json
        else
          [].to_json
        end
      end
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end

  def indexes
    begin
      user_id,_ = get_user

      id = params[:id]
      tables = params[:tables]

      raise 'Diagram not found' if tables.nil? || id.nil?

      tables = [tables] unless tables.kind_of?(Array)

      doc = Docs.with_pk!(id)

      SequelHelper.connection(JSON.parse(doc.connect)) do |db|
        SequelHelper.indexes(db, tables).to_json
      end
    rescue Exception => e
      halt 500, {:error => e.message}.to_json
    end
  end
end