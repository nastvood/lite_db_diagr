class Site < E
  map '/'

  def index
    session[:init] = true unless session.loaded?

    #p Docs.all

    @user_id,@user = AppHelpers.get_user session, false

    render 'index'
  end
end

