module AppHelpers
  def self.get_error_s errors
    errors.values[0][0]
  end

  def self.get_user session, excp = true
    session[:init] = true unless session.loaded?

    if session[:user_id].nil? || session[:user].nil?
      raise 'User not found' if excp

      return 0,nil
    end

    return session[:user_id],session[:user]
  end
end
