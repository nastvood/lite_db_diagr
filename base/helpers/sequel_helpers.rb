module SequelHelper
  class Mysql2Helper
    def self.foreign_keys db,tables
      tables_str = tables.to_s().gsub('[','(').gsub(']',')').gsub('"','\'')

      sql = "SELECT kcu.constraint_name as name,kcu.table_name tableName, kcu.column_name fieldName,
                    kcu.referenced_table_name refTableName, kcu.referenced_column_name refFieldName
                  FROM  information_schema.table_constraints AS tc
                    JOIN information_schema.key_column_usage AS kcu
                      ON tc.constraint_name = kcu.constraint_name
				                AND tc.constraint_schema = kcu.constraint_schema
				                AND tc.table_name = kcu.table_name
                  WHERE tc.constraint_schema = DATABASE()
                    AND constraint_type = \'FOREIGN KEY\'
                    AND tc.table_name in #{tables_str}"

      db.fetch(sql).all
    end

    def self.indexes db, tables
      tables_str = tables.to_s().gsub('[','(').gsub(']',')').gsub('"','\'')

      sql = "SELECT s.table_name as tableName,s.index_name as indexName,
                  s.non_unique as uniq,s.column_name as fieldName, s.seq_in_index as pos,
                  IFNULL(tc.constraint_type,'INDEX') type
                FROM information_schema.statistics s LEFT JOIN information_schema.table_constraints tc
                  ON s.index_name = tc.constraint_name
                    AND s.table_name = tc.table_name
                    AND s.table_schema = tc.table_schema
                WHERE s.table_schema = DATABASE()
                  AND s.table_name IN #{tables_str}
                ORDER BY s.table_name, s.index_name, s.seq_in_index"

      all = db.fetch(sql).all

      table_indexes = []

      table_group = all.group_by {|t| t[:tableName]}
      table_group.each { |t,tv|
        indexes_defs = []
        tv.group_by{|i| i[:indexName]}.each {|i,iv|
          indexes_defs << {
              :name => i,
              :type => iv[0][:type],
              :props => iv.inject([]){|fields,line|
                fields << {
                    :fieldName => line[:fieldName],
                    :uniq => line[:uniq],
                    :pos => line[:pos]
                }
              }
          }
        }

        table_indexes << {
            :name => t,
            :indexes => indexes_defs
        }
      }

      table_indexes
    end
  end

  class Adapters
    NONE = 0
    TINYTDS = 1
    MYSQL2 = 2

    def self.to_s adapter
      case adapter.to_i
        when self::TINYTDS
          "tinytds"
        when self::MYSQL2
          "mysql2"
        else
          ""
      end
    end

    def self.from_sym adapter
      case adapter
        when :tinytds
          self::TINYTDS
        when :mysql2
          self::MYSQL2
        else
          self::NONE
      end
    end

    def self.get_adapter_class adapter
      case self.from_sym(adapter)
        when self::MYSQL2
          Mysql2Helper
        else
          nil
      end
    end

    def self.to_json
      [MYSQL2,TINYTDS].map{|adapter| {:id=>adapter,:name=>self.to_s(adapter)}}.to_json
    end
  end

  def self.connection(conn,&block)
      Sequel.connect :adapter=>SequelHelper::Adapters::to_s(conn["adapter"]),
                     :host=>conn["host"],:port=>conn["port"],:database=>conn["dbname"],
                     :username=>conn["user"],:password=>conn["pass"], :test => true do |db|
        if block_given?
          response = block.call(db)
          db.disconnect

          response
        else
          db
        end
      end
  end

  def self.foreign_keys db, tables
    adapter_class = Adapters.get_adapter_class db.adapter_scheme

    adapter_class.foreign_keys db, tables
  end

  def self.indexes db, tables
    adapter_class = Adapters.get_adapter_class db.adapter_scheme

    adapter_class.indexes db, tables
  end
end