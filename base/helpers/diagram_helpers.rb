class DiagramHelpers
  def self.tables_list diagram
    begin
      tables = []
      JSON.parse(diagram)['tables'].each {|table|
        tables << table['name'] if table.has_key?('name')
      }

      tables
    rescue
      []
    end
  end
end