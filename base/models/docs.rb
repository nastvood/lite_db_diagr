class Docs < Sequel::Model
  many_to_one :user, :class=>:Users

  plugin :timestamps
  plugin :validation_helpers
  def validate
    super
    validates_presence :title, presence: true,:message => 'Title is not empty'
    validates_unique [:user_id,:title], :message => 'Diagram already exist'
  end
end