class Users < Sequel::Model
  one_to_many :docs

  plugin :timestamps
  plugin :validation_helpers
  def validate
    super
    validates_presence :email, presence: true,:message => 'Email is not empty'
    validates_unique :email, :message => 'User already exist'
    validates_format  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :email, :message=> 'Email is invalid'
  end
end