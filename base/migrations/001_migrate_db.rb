Sequel.migration do
  up do
    create_table(:users) do
      primary_key :id_user

      String :email
      String :pass

      DateTime :created_at
      DateTime :updated_at
    end

    create_table(:docs) do
      primary_key :id_doc

      String :title
      Text :connect
      Text :diagram

      DateTime :created_at
      DateTime :updated_at
    end

    alter_table(:users){add_index :email,unique: true}
    alter_table(:docs){add_foreign_key :user_id,:users}
    alter_table(:docs){add_index [:user_id,:title],unique: true}
  end

  down do
    drop_table(:users)
  end
end